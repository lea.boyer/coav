#ifndef CFG_H
#define CFG_H
#include <gcc-plugin.h>
#include <plugin-version.h>
#include <tree-pass.h>
#include <function.h>
#include <system.h>
// Include the global context 'g'.
#include <context.h>
#include <tree.h>
#include <gimple.h>
#include <gimple-iterator.h>
#include "mpi_utils.h"



// Update the bitmap with the arcs of the basic_block graph which don't lead to a circle in the graph
void rec_delete_cycle( bitmap_head *graph,bitmap_head seen, basic_block b);
void delete_cycle(bitmap_head* graph);

// Attribute a rank to each basic block, depending on the maximum number of MPI calls the thread has to go through to reach this bb
int rec_ranking(bitmap_head seen, basic_block b, int* rank, bitmap_head *graph_no_cycle, int r_max);
int ranking(function* fun,  int* rank, bitmap_head *graph_no_cycle);

// Create set of basic_block with the same rank
void set_rank(function* fun,int* rank, bitmap_head* set_rank);

#endif
