#ifndef MPI_UTILS_H
#define MPI_UTILS_H

// #include <gcc-plugin.h>
// #include <plugin-version.h>
// #include <tree-pass.h>
// #include <function.h>
// #include <system.h>
// // Include the global context 'g'.
// #include <context.h>
// #include <tree.h>
// #include <gimple.h>
// #include <gimple-iterator.h>
// #include <c-family/c-pragma.h>
// #include <vec.h>
// #include <coretypes.h>
// #include <errors.h>

#define DEFMPICOLLECTIVES(CODE, NAME) CODE,
enum mpi_collective_code {
#include "../MPI_collectives.def"
    LAST_AND_UNUSED_MPI_COLLECTIVE_CODE
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME) NAME,
const char* const mpi_collective_name[] = {
#include "../MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME,ID) ID,
const int mpi_collective_id[] = {
#include "../MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

/*** mpi_utils includes all the function linked to MPI calls. ***/

// Free the field of each basic_block
void clean_aux_field( function * fun, long val );

// Verify if the basic_block contains a MPI call
enum mpi_collective_code is_mpi_call( gimple * stmt, int bb_index);

// Read all the basic_block and print if they contain a MPI CALL
void read_and_mark_mpi(function *fun);

// Returns the number of MPI calls in a basic_block
int get_nb_mpi_calls_in_bb( basic_block bb );

// Returns true if a basic_clock contains multiple MPi calls, false in the other case
bool check_multiple_mpi_calls_per_bb(function *fun);

// If some basic_blocks contain X MPI calls, it will split them in X differents basic_block, each with only one MPI call
void split_multiple_mpi_calls( function * fun );

// If the function contains basic_blocks with multiple MPI call, calls spli_mutliple_mpi_calls ( function *fun);
void separate_mpi(function *fun, basic_block bb);

#endif
