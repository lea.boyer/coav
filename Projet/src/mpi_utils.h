#ifndef MPI_UTILS_H
#define MPI_UTILS_H

#include <gcc-plugin.h>
#include <plugin-version.h>
#include <tree.h>
#include <tree-pass.h>
#include <c-family/c-pragma.h>
#include <function.h>
#include <system.h>
// Include the global context 'g'.
#include <context.h>
#include <gimple.h>
#include <gimple-iterator.h>
#include <dominance.h>
#include <unistd.h>

#define DEFMPICOLLECTIVES(CODE, NAME,ID) CODE,
enum mpi_collective_code {
#include "MPI_collectives.def"
    LAST_AND_UNUSED_MPI_COLLECTIVE_CODE
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME,ID) NAME,
const char* const mpi_collective_name[] = {
#include "MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME,ID) ID,
const int mpi_collective_id[] = {
#include "MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

/*** mpi_utils includes all the function linked to MPI calls. ***/


// Free the field of each basic_block
void clean_aux_field( function * fun, long val )
{
  basic_block bb; 

  /* Traverse all BBs to clean 'aux' field */
    FOR_ALL_BB_FN (bb, fun)
    {
      bb->aux = (void *)val ;
    }

}

enum mpi_collective_code is_mpi_call( gimple * stmt, int bb_index)
{
	enum mpi_collective_code returned_code ;

	returned_code = LAST_AND_UNUSED_MPI_COLLECTIVE_CODE ;

	if (is_gimple_call (stmt))
	{
		tree t ;
		const char * callee_name ;
		int i ;
		bool found = false ;

		t = gimple_call_fndecl( stmt ) ;
		callee_name = IDENTIFIER_POINTER(DECL_NAME(t)) ;

		i = 0 ;
		while ( !found && i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
		{
			if ( strncmp( callee_name, mpi_collective_name[i], strlen(
							mpi_collective_name[i] ) ) == 0 )
			{
				found = true ;
				returned_code = (enum mpi_collective_code) i ;
			}
			i++ ;
		} 

	}


	// if ( returned_code != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
	// {
	// 	printf( "    -->   |||++||| BB %d [MPI CALL] Found call to %s\n", bb_index, mpi_collective_name[returned_code] ) ;
	// }

	return returned_code ;


}

// Read all the basic_block and print if they contain a MPI CALL
void read_and_mark_mpi(function *fun)
    {
      basic_block bb;
      gimple_stmt_iterator gsi;
      gimple *stmt;

      FOR_EACH_BB_FN(bb,fun)
      {
        for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
        {
          
          stmt = gsi_stmt(gsi);

          enum mpi_collective_code c;
          c = is_mpi_call(stmt, bb->index);
          if(bb->aux == (void *) LAST_AND_UNUSED_MPI_COLLECTIVE_CODE)
          {
            bb->aux = (void *) c ;
          }
        }	
      }

    }

// Returns the number of MPI calls in a basic_block
  int get_nb_mpi_calls_in_bb( basic_block bb )
  {
		gimple_stmt_iterator gsi;
		int nb_mpi_coll = 0 ;

		for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
		{
			gimple *stmt = gsi_stmt (gsi); 

			enum mpi_collective_code c ;
			c = is_mpi_call( stmt, bb->index ) ;

			if ( c != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
			{
				nb_mpi_coll++ ;
			}
		}

		// printf( " == BB %d == contains %d MPI collective(s)\n", 
		// 		bb->index, nb_mpi_coll ) ;

		return nb_mpi_coll ;
  }

// Returns true if a basic_clock contains multiple MPi calls, false in the other case
bool check_multiple_mpi_calls_per_bb(function *fun){
	basic_block bb;
	bool is_multiple_mpi_coll = false;
	int nb_mpi_coll_in_bb = 0;
	FOR_EACH_BB_FN (bb, fun)
	{
		nb_mpi_coll_in_bb = get_nb_mpi_calls_in_bb(bb);	
		if(nb_mpi_coll_in_bb > 1){
			is_multiple_mpi_coll = true;
		}
	}
	return is_multiple_mpi_coll;
  }

// If some basic_blocks contain X MPI calls, it will split them in X differents basic_block, each with only one MPI call
  void split_multiple_mpi_calls( function * fun )
  {
    basic_block bb; 

    FOR_EACH_BB_FN (bb, fun)
    {
      int n = get_nb_mpi_calls_in_bb( bb ) ;

      if ( n > 1 ) 
      {
        gimple_stmt_iterator gsi;

        printf( "[SPLIT] Need to split BB %d (%d collectives)\n",
            bb->index, n ) ;
        for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
        {
          gimple *stmt = gsi_stmt (gsi); 
          enum mpi_collective_code c ;

          c = is_mpi_call( stmt, bb->index ) ;

          if ( c != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
          {
            split_block( bb, stmt ) ;
          }

        }
      }
    }
  }

// If the function contains basic_blocks with multiple MPI call, calls spli_mutliple_mpi_calls ( function *fun);
  bool separate_mpi(function *fun, basic_block bb)
{
  int count = 0;
    for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
      const gimple* stmt = gsi_stmt(gsi);

      if (is_gimple_call(stmt)) {
        const tree t = gimple_call_fndecl(stmt);
        const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));

        for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
          count += (strcmp(mpi_collective_name[i], ident) == 0);

          if (count >= 2) {
            gsi_prev(&gsi);
            gimple* prev_stmt = gsi_stmt(gsi);
            split_block(bb, prev_stmt);
            return true;
          }
        }
      }
    }
    return false;
}

bool split_bb_if_necessary(basic_block bb) {
  int count = 0;
  for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
    const gimple* stmt = gsi_stmt(gsi);

    if (is_gimple_call(stmt)) {
      const tree t = gimple_call_fndecl(stmt);
      const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));

      for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
        count += (strcmp(mpi_collective_name[i], ident) == 0);

        if (count >= 2) {
          gsi_prev(&gsi);
          gimple* prev_stmt = gsi_stmt(gsi);
          split_block(bb, prev_stmt);
          return true;
        }
      }
    }
  }

  return false;
}

static enum mpi_collective_code get_mpi_collective_stmt(const gimple* const stmt) {
    if (is_gimple_call(stmt)) {
        const tree t = gimple_call_fndecl(stmt);
        const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));
        for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
            if (strcmp(mpi_collective_name[i], ident) == 0) {
                return (enum mpi_collective_code)i;
            }
        }
    }

    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}

enum mpi_collective_code get_mpi_collective_bb(const basic_block bb) {
    // Iterates the statement in the basic block. If one is a collective call,
    // return its code.
    for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi);
         gsi_next(&gsi)) {
        const gimple* stmt = gsi_stmt(gsi);

        const enum mpi_collective_code code = get_mpi_collective_stmt(stmt);
        if (code != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
            return code;
        }
    }

    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}

#endif
