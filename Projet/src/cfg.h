#ifndef CFG_H
#define CFG_H

#include <bitmap.h>
#include "mpi_utils.h"


void rec_delete_cycle( bitmap_head *cfg,bitmap_head seen, basic_block b)
  {
    edge p; edge_iterator ei;
    bitmap_set_bit(&seen,b->index);
    FOR_EACH_EDGE(p,ei,b->succs){
      bitmap_head local_seen;
      bitmap_initialize(&local_seen, &bitmap_default_obstack);
      bitmap_copy(&local_seen, &seen);
      // if we haven't visited the node yet, we add it
      // if we did, we don't add it, and so avoid cycle
      if(!bitmap_bit_p(&seen,p->dest->index))
      {
        bitmap_set_bit(&cfg[b->index], p->dest->index);
        rec_delete_cycle(cfg, local_seen,p->dest);
      }
      bitmap_clear(&local_seen);
    }

  }

  // delete cycle in th graph
  void delete_cycle(bitmap_head* cfg)
  {
    bitmap_head seen;
    // basic_bloc already visited
    bitmap_initialize(&seen, &bitmap_default_obstack);
    basic_block b = ENTRY_BLOCK_PTR_FOR_FN(cfun);
    rec_delete_cycle(cfg,seen,b);
    bitmap_clear(&seen);

  }

basic_block get_bb_from_index(function* fun, int index){
  basic_block bb;
  FOR_ALL_BB_FN(bb,cfun){
    if(bb->index == index) return bb;
  }
  return bb;
}

 void rec_ranking(bitmap_head seen, basic_block b, int** rank, bitmap_head *graph_no_cycle)
  {
  read_and_mark_mpi(cfun); 
  edge p; edge_iterator ei;
  int coll;
  bitmap_set_bit(&seen,b->index);
  FOR_EACH_EDGE(p,ei,b->succs){
    bitmap_head local_seen;
    bitmap_initialize(&local_seen, &bitmap_default_obstack);
    bitmap_copy(&local_seen, &seen);
    if((!bitmap_bit_p(&seen,p->dest->index)) && (bitmap_bit_p(&graph_no_cycle[b->index],p->dest->index)))
    {
      for(int i=0; i< LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;i++){
        // the rank is the max between the old basic_block rank, and the new rank which is its predecessor one.
        rank[p->dest->index][i]= max(rank[p->dest->index][i],rank[b->index][i]);
      }
      // we increment the rank on the collective met
      if(get_nb_mpi_calls_in_bb(p->dest)>0){
        coll = mpi_collective_id[get_mpi_collective_bb(p->dest)];
        rank[p->dest->index][coll]++;
      }
        
      rec_ranking(local_seen,p->dest, rank, graph_no_cycle);
      
    }
    bitmap_clear(&local_seen);
  }
}
  

void ranking(function* fun,  int** rank, bitmap_head *graph_no_cycle){ //renvoie l'index max ausi
    basic_block b = ENTRY_BLOCK_PTR_FOR_FN(cfun); 
    bitmap_head seen;
    bitmap_initialize(&seen, &bitmap_default_obstack);
    rec_ranking(seen,b,rank, graph_no_cycle);
    bitmap_clear(&seen);
}

// Create set of basic_block with the same rank
int set_rank(function* fun,int** rank, bitmap_head* set_rank){
  basic_block bb;
  int current_rank_max=0;
  int seen[last_basic_block_for_fn(fun)];
  bool diff=false;
  for(int i=0;i< last_basic_block_for_fn(fun);i++){
    seen[i]=0;
  }
  bitmap_initialize(&set_rank[current_rank_max], &bitmap_default_obstack);

  FOR_ALL_BB_FN(bb,cfun){
    if(!seen[bb->index] && get_nb_mpi_calls_in_bb(bb)>0){
      seen[bb->index]=1;
      bitmap_set_bit(&set_rank[current_rank_max],bb->index);
      
      for(int i =0; i< last_basic_block_for_fn(fun);i++){
        diff = false;
        for(int coll=0;coll<LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;coll++ ){
          if(rank[i][coll]!=rank[bb->index][coll]){
            diff =true;
            break;
          }
        }
        if(!diff && get_nb_mpi_calls_in_bb(get_bb_from_index(fun,i))>0){
          bitmap_set_bit(&set_rank[current_rank_max],i);
          seen[i]=1;
        }
      }
      current_rank_max++;
      bitmap_initialize(&set_rank[current_rank_max], &bitmap_default_obstack);
    }
  }
  return current_rank_max-1;
}

#endif
