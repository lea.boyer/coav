//ici faire les fonctions relatives à la partie 2
#ifndef DIRECTIVES_H
#define DIRECTIVES_H

#include "mpi_utils.h"
#include <vec.h>
#include <coretypes.h>
#include <errors.h>
#include <diagnostic.h>


//global variable
vec<const unsigned char*> pragma_functions={};

/*function to handle pragma directives*/
static void handle_pragma(cpp_reader* ARG_UNUSED(dummy)) {
        enum cpp_ttype token;
        tree x;
        bool need_closing_parenthese = false;
        bool valide_pragma = true;
        /*temporary vec that contains all the function's name that are in pragma directive and are valide*/
	vec<const unsigned char*> functions={};

        if (cfun) {
                error("#pragma ProjetCA option is not allowed inside functions");
                return;
        }

        token = pragma_lex(&x);
        /*Checking if there is any open parenthese*/
        if (token == CPP_OPEN_PAREN) {
                need_closing_parenthese = true;
                token = pragma_lex(&x);
        }

        while (token == CPP_NAME) {
                printf("function name is: %s \n",x->identifier.id.str);
                functions.safe_push(x->identifier.id.str);
		do {
                        token=pragma_lex(&x);
                }while(token == CPP_COMMA);
        }

        if ((need_closing_parenthese) && (token != CPP_CLOSE_PAREN)) {
                printf("#pragma ProjetCA (name1[, name2]...) is missing a closing ')'");
                valide_pragma=false;
        }

        token = pragma_lex(&x);
        if (token != CPP_EOF) {
                printf("#pragma ProjetCA is badly formed");
                valide_pragma=false;
        }
        if(valide_pragma){
	        while(!(functions.is_empty())){
		        const unsigned char* name = functions.pop();
			if(pragma_functions.contains(name)){
				fprintf(stderr, "%s: warning: '%s' is duplicated in '#pragma ProjetCA ...'\n", progname, name);
			} else {
				pragma_functions.safe_push(name);
				printf("pragma_functions length : %d \n",pragma_functions.length());
				printf("%s is in list \n",name);
			}
		}
	}
}

/*function to register pragma directive to be called in the initialisation of plugin*/
static void register_my_pragma (void*, void*) {
        c_register_pragma ("ProjetCA", "mpicoll_check", handle_pragma);
        printf("new pragma \n");
}


#endif
