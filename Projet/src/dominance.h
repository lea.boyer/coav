#ifndef DOMINANCE_H
#define DOMINANCE_H

#include "cfg.h"


// should be call after calculate_dominance_info (CDI_POST_DOMINATORS); and before free_dominance_info (CDI_POST_DOMINATORS);
  // add to frontiers the psf of every basic_block
  // problem : can't usr graph_no_cycle
  void compute_post_dominance_frontiers(bitmap_head *frontiers, bitmap_head *graph_no_cycle)
      {     
      edge p;
      edge_iterator ei;
      basic_block b;
      FOR_EACH_BB_FN (b, cfun)
      {
        if (EDGE_COUNT (b->succs) >= 2)
        {
          basic_block domsb = get_immediate_dominator (CDI_POST_DOMINATORS, b);
          FOR_EACH_EDGE (p, ei, b->succs)
          {
          basic_block runner = p->dest;
            
              
            if (runner == EXIT_BLOCK_PTR_FOR_FN (cfun))
              continue;

            while (runner != domsb)
            {
              if (!bitmap_set_bit (&frontiers[runner->index], b->index))
                break;
              runner = get_immediate_dominator (CDI_POST_DOMINATORS, runner);
            }
          
          }
        }
      }

  }

// Searches the post dominance frontiers of a set of basic_block with the same rank MPI call, and update a bitmap
 void set_post_domine(function* fun, bitmap_head* cfg, bitmap_head* set_r, bitmap_head* frontiers_rank, int max_rank){
    bitmap_head* frontiers  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    calculate_dominance_info (CDI_POST_DOMINATORS);
    for(int i=0; i<=last_basic_block_for_fn(fun);i++){
      bitmap_initialize(&frontiers[i], &bitmap_default_obstack);
    }
    compute_post_dominance_frontiers(frontiers,cfg);

    for(int r=0; r<= max_rank; r++){ //parcours des rangs
      for(int i=0; i<= last_basic_block_for_fn(fun); i++){ //parcours des block 
        if(bitmap_bit_p(&set_r[r],i)){
          for(int j=0; j<= last_basic_block_for_fn(fun); j++){//on ajoute les postdominants au rang
            if(bitmap_bit_p(&frontiers[i],j)){
              bitmap_set_bit(&frontiers_rank[r], j);
            }
          }
        } //si le block a le bon rang
      }
    }
    // free(frontiers);
    free_dominance_info (CDI_POST_DOMINATORS);
  }

void bitmap_union(bitmap_head *one, bitmap_head *two, int row, int col){
    for(int i=0; i<row;i++){
      for(int j=0; j<col;j++){
        if(bitmap_bit_p(&two[i],j))
          bitmap_set_bit(&one[i],j);
      }
    }
  }

  bool bitmap_compare(bitmap_head* one, bitmap_head* two, int row, int col){
     for(int i=0; i<row;i++){
      for(int j=0; j<col;j++){
        if((bitmap_bit_p(&two[i],j)&& !bitmap_bit_p(&one[i],j)) || (!bitmap_bit_p(&two[i],j)&& bitmap_bit_p(&one[i],j)))
          return false;
      }
    }
    return true;
  }

  bool bitmap_empty(bitmap_head* one, int row, int col){
    for(int i=0; i<row;i++){
      for(int j=0; j<col;j++){
        if((bitmap_bit_p(&one[i],j))){
          return false;
      }
    }
  }
  return true;
  }

  // int set_post_domine_i(function* fun, bitmap_head* cfg, bitmap_head* set_r, bitmap_head* frontiers_i_rank, int max_rank){
  //   bitmap_head*  tmp  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
  //   bitmap_head*  tmp_2  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));

    
  //   basic_block bb;
  //   for(int i=0; i<=max_rank;i++){
  //     bitmap_initialize(&tmp[i], &bitmap_default_obstack);
  //     bitmap_initialize(&tmp_2[i], &bitmap_default_obstack);
  //   }
  //   set_post_domine(fun,cfg,set_r, frontiers_i_rank,max_rank);

  //   while(!bitmap_compare(tmp_2,tmp,max_rank,last_basic_block_for_fn(fun))){
  //     FOR_EACH_BB_FN(bb, fun)
  //     {
  //     bitmap_clear(&tmp[bb->index]);
  //     }
  //     bitmap_union(tmp_2,tmp,max_rank,last_basic_block_for_fn(fun));
  //     FOR_EACH_BB_FN(bb, fun)
  //     {
  //     bitmap_clear(&tmp[bb->index]);
  //     }
  //     set_post_domine(fun,cfg, frontiers_i_rank,tmp, max_rank);
  //     bitmap_union(frontiers_i_rank,tmp,max_rank,last_basic_block_for_fn(fun));
  //     for(int j=0; j<=max_rank;j++)
  //     {
  //     printf("\n rang :%d -> ",j);
  //         for(int i=0; i<= last_basic_block_for_fn(fun); i++){
  //             if(bitmap_bit_p(&frontiers_i_rank[j],i)){
  //               printf(" %d ",i);
  //             }
  //         }
  //     }
  //     for(int j=0; j<=max_rank;j++)
  //     {
  //     printf("\n rang :%d -> ",j);
  //         for(int i=0; i<= last_basic_block_for_fn(fun); i++){
  //             if(bitmap_bit_p(&tmp[j],i)){
  //               printf(" %d ",i);
  //             }
  //         }
  //     }
  //     if (bitmap_empty(tmp, max_rank, last_basic_block_for_fn(fun))){
  //           FOR_EACH_BB_FN(bb, fun)
  //       {
  //         bitmap_clear(&tmp[bb->index]);
  //       }
  //       free(tmp);
  //       return true;
  //     }

  //   }
  //   FOR_EACH_BB_FN(bb, fun)
  //   {
  //     bitmap_clear(&tmp[bb->index]);
  //   }
  //   free(tmp);
  //   return false;

  // }

// If a set of asic_block with the same rank MPI call have mutliple post dominance frontiers, raises a warning : a thread can take a path without executing a MPI call
  void warning_mpi(function* fun, bitmap_head* frontiers_rank, int max_r, int empty){
    printf("\n");
    if(empty){
      printf("Pas d'erreur MPI. \n");
      return ;
    }
   
    for(int i=0; i<max_r;i++){
      for(int j=0;j <= last_basic_block_for_fn(fun); j++){
        if(bitmap_bit_p(&frontiers_rank[i],j)){
          printf("Attention il y a une erreur, le bloc %d ne reçoit pas la collective du bloc %d",i,j);
        }
      }
    }
  }

#endif
