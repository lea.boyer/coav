//#include "src/dominance.h"
//#include "src/cfg.h"
#include "src/directives.h"
#include "src/dominance.h"

const pass_data my_pass_data = {
    .type = GIMPLE_PASS,
    .name = "CA_PROJET",
    .optinfo_flags = OPTGROUP_NONE,
    .tv_id = TV_OPTIMIZE,
    .properties_required = 0,
    .properties_provided = 0,
    .properties_destroyed = 0,
    .todo_flags_start = 0,
    .todo_flags_finish = 0,
};

class my_pass : public gimple_opt_pass {
public:
    my_pass(gcc::context* ctxt)
        : gimple_opt_pass(my_pass_data, ctxt)
    {
    }

    my_pass* clone() { return new my_pass(g); }

    bool gate(function*) { 
        const unsigned char* name = (const unsigned char*) function_name(cfun);
        for(int i=0;i<(int) pragma_functions.length();i++){
            if(name == pragma_functions[i]){
                pragma_functions.unordered_remove((unsigned) i);
                printf("%s is the actual function! \n", name);
	            return true;
            }
        }
        return false;

    }

	unsigned int execute(function* fun)
    {
    // Print all the basic_block with a MPI call
    read_and_mark_mpi(fun);

    basic_block bb;
    FOR_ALL_BB_FN(bb,fun){
      separate_mpi(fun,bb);
    }


   
    // Create a new cfg from the function cfg, but without circle in it
    bitmap_head* cfg;
    cfg  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    FOR_ALL_BB_FN(bb,fun){
      bitmap_initialize(&cfg[bb->index], &bitmap_default_obstack);
    }
    
    delete_cycle(cfg);

    printf("\n Deleting Circle : \n");
    FOR_EACH_BB_FN(bb, fun)
    {
    printf("\n%d -> ",bb->index);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&cfg[bb->index],i)){
              printf(" %d ",i);
            }
        }
    }

// Calculate the post dominance frontier of each basic_block
    bitmap_head *pdfs;
    calculate_dominance_info (CDI_POST_DOMINATORS);
    pdfs = XNEWVEC (bitmap_head, last_basic_block_for_fn (cfun));
    FOR_EACH_BB_FN (bb, fun)
        bitmap_initialize (&pdfs[bb->index], &bitmap_default_obstack);
    compute_post_dominance_frontiers (pdfs,cfg);

    FOR_EACH_BB_FN(bb, fun)
    {  
        printf("Post Dominance Frontier of %d :\n",bb->index); 
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&pdfs[bb->index],i)){
            printf("%d\n",i);
            }
        }
    }
    free_dominance_info (CDI_POST_DOMINATORS);
    free(pdfs);

    // Rank each basic block depending on the maximum number of MPI calls the thread has to go through to reach this bb
    printf("\n Ranking : \n");
    int** rank = (int**) xmalloc(last_basic_block_for_fn(fun)*sizeof(*rank));
    for(int i =0; i < last_basic_block_for_fn(fun); i++){
        rank[i]= (int*) xmalloc( LAST_AND_UNUSED_MPI_COLLECTIVE_CODE *sizeof(**rank));
        for(int j=0;j<LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;j++){
          rank[i][j]=0;
        }
      }
    
    ranking(fun,rank,cfg);
    FOR_EACH_BB_FN(bb, fun)
    {
      printf("Bloc : %d \n",bb->index);
      for(int i =0; i< LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;i++)
        printf("coll : %d, nbr = %d | ", i, rank[bb->index][i]);
      printf("\n");
    }

    
    // Create set of basic_block with the same rank
    printf("\n Set of basic_block with the same rank : \n");
    bitmap_head* set_r  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    int nb_set = set_rank(fun,rank,set_r);

    for(int j=0; j<=nb_set;j++)
    {
    printf("\n rang :%d -> ",j);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&set_r[j],i)){
              printf(" %d ",i);
            }
        }
    }

     // Searches the post dominance frontiers of a set of basic_block with the same rank MPI call
    printf("\n Post-dominance frontier of sets: \n");
    bitmap_head* frontiers_rank = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));

     for(int i=0; i<=nb_set;i++){
      bitmap_initialize(&frontiers_rank[i], &bitmap_default_obstack);    }
    set_post_domine(fun,cfg,set_r,frontiers_rank,nb_set);
     for(int j=0; j<=nb_set;j++)
    {
    printf("\n rang :%d -> ",j);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&frontiers_rank[j],i)){
              printf(" %d ",i);
            }
        }
    }

    //Searches the post dominance frontiers of a set of basic_block with the same rank MPI call
    printf("\n Post-dominance frontier iteree of sets: \n");

    printf("\n To see the post-dominance frontier working, go on MPI_sequence folder. \n");
    // bitmap_head* frontiers_i_rank = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));

    //  for(int i=0; i<=nb_set;i++){
    //   bitmap_initialize(&frontiers_i_rank[i], &bitmap_default_obstack);
    // }
    // int empty =set_post_domine_i(fun,cfg,set_r,frontiers_i_rank,nb_set);
    
    //  for(int j=0; j<=nb_set;j++)
    // {
    // printf("\n rang :%d -> ",j);
    //     for(int i=0; i<= last_basic_block_for_fn(fun); i++){
    //         if(bitmap_bit_p(&frontiers_i_rank[j],i)){
    //           printf(" %d ",i);
    //         }
    //     }
    // }





    // Raise a warning if a set of asic_block with the same rank MPI call have mutliple post dominance frontiers : a thread can take a path without executing a MPI call
    //warning_mpi(fun,frontiers_rank,nb_set,empty);
    free(cfg);
    free(frontiers_rank);
    // free(frontiers_i_rank);

    for(int i=0;i<last_basic_block_for_fn(fun);i++){
      free(rank[i]);
    }

    return 0;
  }
};

/* ========================================================================== */

/* Global variable required for plugin to execute */
int plugin_is_GPL_compatible = 1;

/* Main entry point for plugin */
int plugin_init(struct plugin_name_args* plugin_info, struct plugin_gcc_version* version){
    if (!plugin_default_version_check(version, &gcc_version)) {
        return 1;
    }

    my_pass pass_inst { g };
    const char *plugin_name = plugin_info->base_name;

    struct register_pass_info pass_info = {
        .pass = &pass_inst,
        .reference_pass_name = "cfg",
        .ref_pass_instance_number = 0,
        .pos_op = PASS_POS_INSERT_AFTER,
    };

    register_callback(plugin_name, PLUGIN_PASS_MANAGER_SETUP, NULL, &pass_info);
    
    register_callback (plugin_name, PLUGIN_PRAGMAS, register_my_pragma, NULL);

    return 0;
}
