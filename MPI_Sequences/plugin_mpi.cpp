#include <gcc-plugin.h>
#include <plugin-version.h>
#include <tree-pass.h>
#include <function.h>
#include <system.h>
// Include the global context 'g'.
#include <context.h>
#include <tree.h>
#include <gimple.h>
#include <gimple-iterator.h>
#include <unistd.h>

#define DEFMPICOLLECTIVES(CODE, NAME,ID) CODE,
enum mpi_collective_code {
#include "MPI_collectives.def"
    LAST_AND_UNUSED_MPI_COLLECTIVE_CODE
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME,ID) NAME,
const char* const mpi_collective_name[] = {
#include "MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME,ID) ID,
const int mpi_collective_id[] = {
#include "MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })



const pass_data my_pass_data = {
    .type = GIMPLE_PASS,
    .name = "CA_TD4",
    .optinfo_flags = OPTGROUP_NONE,
    .tv_id = TV_OPTIMIZE,
    .properties_required = 0,
    .properties_provided = 0,
    .properties_destroyed = 0,
    .todo_flags_start = 0,
    .todo_flags_finish = 0,
};

class my_pass : public gimple_opt_pass {
public:
  my_pass(gcc::context* ctxt)
      : gimple_opt_pass(my_pass_data, ctxt)
  {
  }

  my_pass* clone() { return new my_pass(g); }

  bool gate(function*) { return true; }


  void clean_aux_field( function * fun, long val )
  {
	basic_block bb; 

	/* Traverse all BBs to clean 'aux' field */
    FOR_ALL_BB_FN (bb, fun)
    {
      bb->aux = (void *)val ;
    }

  }

  static enum mpi_collective_code get_mpi_collective_stmt(const gimple* const stmt)
{
    if (is_gimple_call(stmt)) {
        const tree t = gimple_call_fndecl(stmt);
        const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));
        for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
            if (strcmp(mpi_collective_name[i], ident) == 0) {
                return (enum mpi_collective_code)i;
            }
        }
    }

    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}


  enum mpi_collective_code get_mpi_collective_bb(const basic_block bb)
{
    // Iterates the statement in the basic block. If one is a collective call,
    // return its code.
    for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi);
         gsi_next(&gsi)) {
        const gimple* stmt = gsi_stmt(gsi);

        const enum mpi_collective_code code = get_mpi_collective_stmt(stmt);
        if (code != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
            return code;
        }
    }

    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}


  // should be call after calculate_dominance_info (CDI_POST_DOMINATORS); and before free_dominance_info (CDI_POST_DOMINATORS);
  // add to frontiers the psf of every basic_block
  // problem : can't usr graph_no_cycle
  void compute_post_dominance_frontiers(bitmap_head *frontiers, bitmap_head *graph_no_cycle)
      {     
      edge p;
      edge_iterator ei;
      basic_block b;
      FOR_EACH_BB_FN (b, cfun)
      {
        if (EDGE_COUNT (b->succs) >= 2)
        {
          basic_block domsb = get_immediate_dominator (CDI_POST_DOMINATORS, b);
          FOR_EACH_EDGE (p, ei, b->succs)
          {
          basic_block runner = p->dest;
            
              
            if (runner == EXIT_BLOCK_PTR_FOR_FN (cfun))
              continue;

            while (runner != domsb)
            {
              if (!bitmap_set_bit (&frontiers[runner->index], b->index))
                break;
              runner = get_immediate_dominator (CDI_POST_DOMINATORS, runner);
            }
          
          }
        }
      }
    }

  void rec_delete_cycle( bitmap_head *cfg,bitmap_head seen, basic_block b)
  {
    edge p; edge_iterator ei;
    bitmap_set_bit(&seen,b->index);
    FOR_EACH_EDGE(p,ei,b->succs){
      bitmap_head local_seen;
      bitmap_initialize(&local_seen, &bitmap_default_obstack);
      bitmap_copy(&local_seen, &seen);
      // if we haven't visited the node yet, we add it
      // if we did, we don't add it, and so avoid cycle
      if(!bitmap_bit_p(&seen,p->dest->index))
      {
        bitmap_set_bit(&cfg[b->index], p->dest->index);
        rec_delete_cycle(cfg, local_seen,p->dest);
      }
      bitmap_clear(&local_seen);
    }

  }

  // delete cycle in th graph
  void delete_cycle(bitmap_head* cfg)
  {
    bitmap_head seen;
    // basic_bloc already visited
    bitmap_initialize(&seen, &bitmap_default_obstack);
    basic_block b = ENTRY_BLOCK_PTR_FOR_FN(cfun);
    rec_delete_cycle(cfg,seen,b);
    bitmap_clear(&seen);

  }

  enum mpi_collective_code is_mpi_call( gimple * stmt, int bb_index)
  {
    enum mpi_collective_code returned_code ;

    returned_code = LAST_AND_UNUSED_MPI_COLLECTIVE_CODE ;

    if (is_gimple_call (stmt))
    {
      tree t ;
      const char * callee_name ;
      int i ;
      bool found = false ;

      t = gimple_call_fndecl( stmt ) ;
      callee_name = IDENTIFIER_POINTER(DECL_NAME(t)) ;

      i = 0 ;
      while ( !found && i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
      {
        if ( strncmp( callee_name, mpi_collective_name[i], strlen(
                mpi_collective_name[i] ) ) == 0 )
        {
          found = true ;
          returned_code = (enum mpi_collective_code) i ;
        }
        i++ ;
      } 

    }


    // if ( returned_code != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
    // {
    //   printf( "    -->   |||++||| BB %d [MPI CALL] Found call to %s\n", bb_index, mpi_collective_name[returned_code] ) ;
    // }

    return returned_code ;


  }


  void read_and_mark_mpi(function *fun)
    {
      basic_block bb;
      gimple_stmt_iterator gsi;
      gimple *stmt;

      FOR_EACH_BB_FN(bb,fun)
      {

        /* Iterate on gimple statements in the current basic block */
        for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
        {
        
          stmt = gsi_stmt(gsi);

          enum mpi_collective_code c;
          c = is_mpi_call(stmt, bb->index);
          if(bb->aux == (void *) LAST_AND_UNUSED_MPI_COLLECTIVE_CODE)
          {
            bb->aux = (void *) c ;
          }

        }	
      }

    }

    

  int get_nb_mpi_calls_in_bb( basic_block bb )
  {
		gimple_stmt_iterator gsi;
		int nb_mpi_coll = 0 ;

		for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
		{
			gimple *stmt = gsi_stmt (gsi); 

			enum mpi_collective_code c ;
			c = is_mpi_call( stmt, bb->index ) ;

			if ( c != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
			{
				nb_mpi_coll++ ;
			}
		}

		// printf( " == BB %d == contains %d MPI collective(s)\n", 
		// 		bb->index, nb_mpi_coll ) ;

		return nb_mpi_coll ;
  }


  bool check_multiple_mpi_calls_per_bb(function *fun)
  {
	basic_block bb;
	bool is_multiple_mpi_coll = false;
	int nb_mpi_coll_in_bb = 0;
	FOR_EACH_BB_FN (bb, fun)
	{
		nb_mpi_coll_in_bb = get_nb_mpi_calls_in_bb(bb);	
		if(nb_mpi_coll_in_bb > 1){
			is_multiple_mpi_coll = true;
		}
	}
	return is_multiple_mpi_coll;
  }

  void split_multiple_mpi_calls( function * fun )
  {
    basic_block bb; 

    FOR_EACH_BB_FN (bb, fun)
    {
      int n = get_nb_mpi_calls_in_bb( bb ) ;

      if ( n > 1 ) 
      {
        gimple_stmt_iterator gsi;

        printf( "[SPLIT] Need to split BB %d (%d collectives)\n",
            bb->index, n ) ;
        for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
        {
          gimple *stmt = gsi_stmt (gsi); 
          enum mpi_collective_code c ;

          c = is_mpi_call( stmt, bb->index ) ;

          if ( c != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE )
          {
            split_block( bb, stmt ) ;
          }

        }
      }
    }
  }

  bool separate_mpi(function *fun, basic_block bb)
  {
  int count = 0;
    for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
      const gimple* stmt = gsi_stmt(gsi);

      if (is_gimple_call(stmt)) {
        const tree t = gimple_call_fndecl(stmt);
        const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));

        for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
          count += (strcmp(mpi_collective_name[i], ident) == 0);

          if (count >= 2) {
            gsi_prev(&gsi);
            gimple* prev_stmt = gsi_stmt(gsi);
            split_block(bb, prev_stmt);
            return true;
          }
        }
      }
    }
  }


  void rec_ranking(bitmap_head seen, basic_block b, int** rank, bitmap_head *graph_no_cycle)
  {
    read_and_mark_mpi(cfun); 
    edge p; edge_iterator ei;
    int coll;
    bitmap_set_bit(&seen,b->index);
    FOR_EACH_EDGE(p,ei,b->succs){
      bitmap_head local_seen;
      bitmap_initialize(&local_seen, &bitmap_default_obstack);
      bitmap_copy(&local_seen, &seen);
      if((!bitmap_bit_p(&seen,p->dest->index)) && (bitmap_bit_p(&graph_no_cycle[b->index],p->dest->index)))
      {
        for(int i=0; i< LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;i++){
          // the rank is the max between the old basic_block rank, and the new rank which is its predecessor one.
          rank[p->dest->index][i]= max(rank[p->dest->index][i],rank[b->index][i]);
        }
        // we increment the rank on the collective met
        if(get_nb_mpi_calls_in_bb(p->dest)>0){
          coll = mpi_collective_id[get_mpi_collective_bb(p->dest)];
          rank[p->dest->index][coll]++;
        }
          
        rec_ranking(local_seen,p->dest, rank, graph_no_cycle);
        
      }
      bitmap_clear(&local_seen);
    }
  }

  // Rank every node depending on how many collectives we have to meet to achieve them
  // A rank is a list with a length of _LAST_AND_UNUSED_MPI_COLLECTIVE_CODE_. We increment a collective_id every time we meet one of them.
  void ranking(function* fun,  int** rank, bitmap_head *graph_no_cycle) 
  {
    basic_block b = ENTRY_BLOCK_PTR_FOR_FN(cfun); 
    bitmap_head seen;
    bitmap_initialize(&seen, &bitmap_default_obstack);
    rec_ranking(seen,b,rank, graph_no_cycle);
    bitmap_clear(&seen);
  }

  // return the basic bloc with the chosen index
  basic_block get_bb_from_index(function* fun, int index){
    basic_block bb;
    FOR_ALL_BB_FN(bb,cfun){
      if(bb->index == index) return bb;
    }
    return bb;
  }

  // create set of ranks
  int set_rank(function* fun,int** rank, bitmap_head* set_rank){
    basic_block bb;
    int current_rank_max=0;
    int seen[last_basic_block_for_fn(fun)];
    bool diff=false;
    for(int i=0;i< last_basic_block_for_fn(fun);i++){
      seen[i]=0;
    }
    bitmap_initialize(&set_rank[current_rank_max], &bitmap_default_obstack);

    FOR_ALL_BB_FN(bb,cfun){
      if(!seen[bb->index] && get_nb_mpi_calls_in_bb(bb)>0){
        seen[bb->index]=1;
        bitmap_set_bit(&set_rank[current_rank_max],bb->index);
       
        for(int i =0; i< last_basic_block_for_fn(fun);i++){
          diff = false;
          for(int coll=0;coll<LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;coll++ ){
            if(rank[i][coll]!=rank[bb->index][coll]){
              diff =true;
              break;
            }
          }
          if(!diff && get_nb_mpi_calls_in_bb(get_bb_from_index(fun,i))>0){
            bitmap_set_bit(&set_rank[current_rank_max],i);
            seen[i]=1;
          }
        }
        current_rank_max++;
        bitmap_initialize(&set_rank[current_rank_max], &bitmap_default_obstack);
      }
    }
    return current_rank_max-1;
  }


  void set_post_domine(function* fun, bitmap_head* cfg, bitmap_head* set_r, bitmap_head* frontiers_rank, int max_rank){
    bitmap_head* frontiers  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    calculate_dominance_info (CDI_POST_DOMINATORS);
    for(int i=0; i<=max_rank;i++){
      bitmap_initialize(&frontiers[i], &bitmap_default_obstack);
    }
    compute_post_dominance_frontiers(frontiers,cfg);

    //for(int i=0; i<set_r.size();i++){printf("\n");}
    //bitmap_ior_into
    for(int r=0; r<= max_rank; r++){ //parcours des rangs
      for(int i=0; i<= last_basic_block_for_fn(fun); i++){ //parcours des block 
        if(bitmap_bit_p(&set_r[r],i)){
          for(int j=0; j<= last_basic_block_for_fn(fun); j++){//on ajoute les postdominants au rang
            if(bitmap_bit_p(&frontiers[i],j)){
              bitmap_set_bit(&frontiers_rank[r], j);
            }
          }
        } //si le block a le bon rang
      }
    }
    free(frontiers);
    free_dominance_info (CDI_POST_DOMINATORS);

  }

  void bitmap_union(bitmap_head *one, bitmap_head *two, int row, int col){
    for(int i=0; i<row;i++){
      for(int j=0; j<col;j++){
        if(bitmap_bit_p(&two[i],j))
          bitmap_set_bit(&one[i],j);
      }
    }
  }

  bool bitmap_compare(bitmap_head* one, bitmap_head* two, int row, int col){
     for(int i=0; i<row;i++){
      for(int j=0; j<col;j++){
        if((bitmap_bit_p(&two[i],j)&& !bitmap_bit_p(&one[i],j)) || (!bitmap_bit_p(&two[i],j)&& bitmap_bit_p(&one[i],j)))
          return false;
      }
    }
    return true;
  }

  bool bitmap_empty(bitmap_head* one, int row, int col){
    for(int i=0; i<row;i++){
      for(int j=0; j<col;j++){
        if((bitmap_bit_p(&one[i],j))){
          return false;
      }
    }
  }
  return true;
}


  int set_post_domine_i(function* fun, bitmap_head* cfg, bitmap_head* set_r, bitmap_head* frontiers_i_rank, int max_rank){
    bitmap_head*  tmp  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    bitmap_head*  tmp_2  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));

    
    basic_block bb;
    for(int i=0; i<=max_rank;i++){
      bitmap_initialize(&tmp[i], &bitmap_default_obstack);
      bitmap_initialize(&tmp_2[i], &bitmap_default_obstack);
    }
    set_post_domine(fun,cfg,set_r, frontiers_i_rank,max_rank);

    while(!bitmap_compare(tmp_2,tmp,max_rank,last_basic_block_for_fn(fun))){
      FOR_EACH_BB_FN(bb, fun)
      {
      bitmap_clear(&tmp[bb->index]);
      }
      bitmap_union(tmp_2,tmp,max_rank,last_basic_block_for_fn(fun));
      FOR_EACH_BB_FN(bb, fun)
      {
      bitmap_clear(&tmp[bb->index]);
      }
      set_post_domine(fun,cfg, frontiers_i_rank,tmp, max_rank);
      bitmap_union(frontiers_i_rank,tmp,max_rank,last_basic_block_for_fn(fun));
      for(int j=0; j<=max_rank;j++)
      {
      printf("\n rang :%d -> ",j);
          for(int i=0; i<= last_basic_block_for_fn(fun); i++){
              if(bitmap_bit_p(&frontiers_i_rank[j],i)){
                printf(" %d ",i);
              }
          }
      }
      for(int j=0; j<=max_rank;j++)
      {
      printf("\n rang :%d -> ",j);
          for(int i=0; i<= last_basic_block_for_fn(fun); i++){
              if(bitmap_bit_p(&tmp[j],i)){
                printf(" %d ",i);
              }
          }
      }
      if (bitmap_empty(tmp, max_rank, last_basic_block_for_fn(fun))){
            FOR_EACH_BB_FN(bb, fun)
        {
          bitmap_clear(&tmp[bb->index]);
        }
        free(tmp);
        return true;
      }

    }
    FOR_EACH_BB_FN(bb, fun)
    {
      bitmap_clear(&tmp[bb->index]);
    }
    free(tmp);
    return false;

  }


  void warning_mpi(function* fun, bitmap_head* frontiers_rank, int max_r, int empty){
    printf("\n");
    if(empty){
      printf("Pas d'erreur MPI. \n");
      return ;
    }
   
    for(int i=0; i<max_r;i++){
      for(int j=0;j <= last_basic_block_for_fn(fun); j++){
        if(bitmap_bit_p(&frontiers_rank[i],j)){
          printf("Attention il y a une erreur, au rang %d avec la collective %d",i,j);
        }
      }
    }
  }
  



	unsigned int execute(function* fun)
    {
    // Print all the basic_block with a MPI call
    read_and_mark_mpi(fun);

    basic_block bb;
    FOR_ALL_BB_FN(bb,fun){
      separate_mpi(fun,bb);
    }


   
    // Create a new cfg from the function cfg, but without circle in it
    bitmap_head* cfg;
    cfg  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    FOR_ALL_BB_FN(bb,fun){
      bitmap_initialize(&cfg[bb->index], &bitmap_default_obstack);
    }
    
    delete_cycle(cfg);

    printf("\n Deleting Circle : \n");
    FOR_EACH_BB_FN(bb, fun)
    {
    printf("\n%d -> ",bb->index);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&cfg[bb->index],i)){
              printf(" %d ",i);
            }
        }
    }

// Calculate the post dominance frontier of each basic_block
    bitmap_head *pdfs;
    calculate_dominance_info (CDI_POST_DOMINATORS);
    pdfs = XNEWVEC (bitmap_head, last_basic_block_for_fn (cfun));
    FOR_EACH_BB_FN (bb, fun)
        bitmap_initialize (&pdfs[bb->index], &bitmap_default_obstack);
    compute_post_dominance_frontiers (pdfs,cfg);

    FOR_EACH_BB_FN(bb, fun)
    {  
        printf("Post Dominance Frontier of %d :\n",bb->index); 
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&pdfs[bb->index],i)){
            printf("%d\n",i);
            }
        }
    }
    free_dominance_info (CDI_POST_DOMINATORS);
    free(pdfs);

    // Rank each basic block depending on the maximum number of MPI calls the thread has to go through to reach this bb
    printf("\n Ranking : \n");
    int** rank = (int**) xmalloc(last_basic_block_for_fn(fun)*sizeof(*rank));
    for(int i =0; i < last_basic_block_for_fn(fun); i++){
        rank[i]= (int*) xmalloc( LAST_AND_UNUSED_MPI_COLLECTIVE_CODE *sizeof(**rank));
        for(int j=0;j<LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;j++){
          rank[i][j]=0;
        }
      }
    
    ranking(fun,rank,cfg);
    FOR_EACH_BB_FN(bb, fun)
    {
      printf("Bloc : %d \n",bb->index);
      for(int i =0; i< LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;i++)
        printf("coll : %d, nbr = %d | ", i, rank[bb->index][i]);
      printf("\n");
    }

    
    // Create set of basic_block with the same rank
    printf("\n Set of basic_block with the same rank : \n");
    bitmap_head* set_r  = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));
    int nb_set = set_rank(fun,rank,set_r);

    for(int j=0; j<=nb_set;j++)
    {
    printf("\n rang :%d -> ",j);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&set_r[j],i)){
              printf(" %d ",i);
            }
        }
    }

    // Searches the post dominance frontiers of a set of basic_block with the same rank MPI call
    printf("\n Post-dominance frontier of sets: \n");
    bitmap_head* frontiers_rank = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));

     for(int i=0; i<=nb_set;i++){
      bitmap_initialize(&frontiers_rank[i], &bitmap_default_obstack);    }
    set_post_domine(fun,cfg,set_r,frontiers_rank,nb_set);
     for(int j=0; j<=nb_set;j++)
    {
    printf("\n rang :%d -> ",j);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&frontiers_rank[j],i)){
              printf(" %d ",i);
            }
        }
    }

    //Searches the post dominance frontiers of a set of basic_block with the same rank MPI call
    printf("\n Post-dominance frontier iteree of sets: \n");
    bitmap_head* frontiers_i_rank = XNEWVEC (bitmap_head, last_basic_block_for_fn (fun));

     for(int i=0; i<=nb_set;i++){
      bitmap_initialize(&frontiers_i_rank[i], &bitmap_default_obstack);
    }
    int empty =set_post_domine_i(fun,cfg,set_r,frontiers_i_rank,nb_set);
    
     for(int j=0; j<=nb_set;j++)
    {
    printf("\n rang :%d -> ",j);
        for(int i=0; i<= last_basic_block_for_fn(fun); i++){
            if(bitmap_bit_p(&frontiers_i_rank[j],i)){
              printf(" %d ",i);
            }
        }
    }



    // Raise a warning if a set of asic_block with the same rank MPI call have mutliple post dominance frontiers : a thread can take a path without executing a MPI call
    warning_mpi(fun,frontiers_rank,nb_set,empty);
    
    // // Check if the collectives are in the same order
    // check_same_collective(fun,max,rank);

    // Cleaning
    FOR_EACH_BB_FN(bb, fun)
    {
        bitmap_clear(&cfg[bb->index]);
  
    }
    // free(cfg);
    // free(set_r);
    // free(frontiers_rank);
    // free(frontiers_i_rank);
    // for(int i=0;i<13;i++){
    //     free(rank[i]);
    // }
    
    return 0;
  }
};

/* ========================================================================== */

/* Global variable required for plugin to execute */
int plugin_is_GPL_compatible = 1;

/* Main entry point for plugin */
int plugin_init(struct plugin_name_args* plugin_info,
    struct plugin_gcc_version* version)
{
    if (!plugin_default_version_check(version, &gcc_version)) {
        return 1;
    }

    my_pass pass_inst { g };

    struct register_pass_info pass_info = {
        .pass = &pass_inst,
        .reference_pass_name = "cfg",
        .ref_pass_instance_number = 0,
        .pos_op = PASS_POS_INSERT_AFTER,
    };

    register_callback(
        plugin_info->base_name,
        PLUGIN_PASS_MANAGER_SETUP,
        NULL,
        &pass_info);

    return 0;
}
